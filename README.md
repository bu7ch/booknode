
### Booknode
--

C'est un petit projet en `node.js`
avec les outils (libs) annexes afin de mieux comprendre l'architecture d'une application avec node.js.

TODO : 

- [x] Le server se lance (express)
- [x] definir une/des route(s)
- [x] faire un model (mongoose)
-  test avec mocha & chai
  - [x] GET all books 
  - [x] POST a book
  - [x] GET/:id a book
  - [x] PUT/:id a book
  - [x] DELETE/:id a book
- [x] connecter à la base de donnée (MongoDB)
- [x] tester avec Insomnia/postman (RESTful tools)
- [x] install template engine
- [x] render page with template engine
