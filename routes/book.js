let mongoose = require("mongoose");
let Book = require("../models/book")

/* 
 GET /book la route pour retrouver tout les livres
*/

getBooks = (req, res) => {
  //requete sur la DB et si pas d'erreurs retourne tout les livres
  let query= Book.find({});
  query.exec((err, books) => {
    if (err) return console.error(err)
    res.render("allbooks",{books:books})
  })
}
/* 
POST a book to save a new book.
*/
getForm = (req, res) => {
  res.render("form")
}
postBook = (req, res) => {
  let newBook = new Book(req.body) // cfg express
  newBook.save((err, book) => {
    if (err){
      res.send(err);
    }
    else {
      // res.json({message : "Book successfully added!", book });
      res.redirect("/book")
    }
  })
}
/* 
GET/book/:id select on book by ID
*/
getBook = (req, res) => {
  Book.findById(req.params.id, (err, book) => {
    if (err) res.send(err)
    res.json(book)
  });
}

/* 
PUT/book/:id to update a book given its id
*/
getEditForm = (req, res) => {
  Book.findById(req.params.id, (err, book) => {
    if(err) res.send(err)
  res.render("edit", {book:book})
    })
  }
updateBook = (req, res) => {
  Book.findById(req.params.id, (err, book) => {
    if(err) res.send(err)
    Object.assign(book, req.body).save((err, book) => {
      if (err) res.send(err)
      // res.json({message: 'Book updated!', book})
      res.redirect('/book')
    });
  });
}
/* 
DELETE /book/:id to delete a book given its id
*/
deleteBook = (req, res) => {
  Book.findByIdAndDelete(req.params.id, (err, result) => {
    // res.json({message: 'Book successfully deleted!',result})
    res.redirect('/book');
  })
}

module.exports = { getBooks, postBook, getBook, updateBook, deleteBook,getForm, getEditForm }