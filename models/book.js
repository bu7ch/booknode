let mongoose = require("mongoose");
let Schema = mongoose.Schema;

// la definition d'un Schema
let BookSchema = new Schema({
  title: { type: String, required: true },
  author: { type: String, required: true },
  year: { type: Number, required: true },
  pages: { type: Number, required: true, min: 1 },
  createdAt: { type: Date, default: Date.now }
},
{
  versionKey: false
}
);
// createdAt defini avec les paramètres de la date courante
BookSchema.pre('save', next => {
  now = new Date();
  if(!this.createdAt) {
    this.createdAt = now
  }
  next()
});

//Export du Shema pour l'utiliser ou on veut
const Book = mongoose.model('Book', BookSchema);

module.exports = Book;