const express = require('express');
const app = express();
const config = require('config');
const mongoose = require('mongoose');
const volleyball = require('volleyball');
const book = require('./routes/book')


app.use(express.static(__dirname + '/public'))
app.set("view engine", "pug")
app.set("views", "./views")

mongoose.connect(config.DBHost,{ useNewUrlParser: true, useUnifiedTopology: true } );
let db = mongoose.connection;
db.on('error',console.error.bind(console, 'conection error:'));
db.once('open',() => {
  console.log(`[MongoDB is ready 💥]`);
});
if (config.util.getEnv("NODE_ENV") !== 'test') {
  app.use(volleyball);
  
}
app.use(express.json())
app.use(express.urlencoded({extended: true}));


const port = config.poulet

app.get('/', (req, res) => {
res.render('home')
})
app.route('/book')
  .get(book.getBooks)
  .post(book.postBook)
app.route('/book/new')
  .get(book.getForm)
app.route('/book/:id')
  .get(book.getBook)
app.route('/book/:id/edit')
  .get(book.getEditForm)
  .post(book.updateBook)
app.route('/book/:id/delete')
  .get(book.deleteBook)

app.listen(port, ()=> console.log(`[ App is on 🔥 on :${port}]`)
);
module.exports = app; //for testing